FROM mcr.microsoft.com/dotnet/core/sdk:3.0-alpine AS build
WORKDIR /src
COPY . .
RUN dotnet build -c Release 
RUN dotnet publish -c Release --no-restore --no-build -o /app

FROM mcr.microsoft.com/dotnet/core/aspnet:3.0-alpine as final
WORKDIR /app
EXPOSE 80
COPY --from=build /app .
ENTRYPOINT ["dotnet", "raw_dotnetcore_3.dll"]