# MacBook Pro (Retina, 13-inch, Early 2015) | 2.7 GHz Intel Core i5 | 16 GB 1867 MHz DDR3
### Running 30s test @ http://localhost:5000/
###   10 threads and 100 connections
###   Thread Stats   Avg      Stdev     Max   +/- Stdev
###     Latency     3.00ms    3.82ms 119.02ms   99.12%
###     Req/Sec     3.59k   310.69     7.55k    93.99%
###   1071314 requests in 30.05s, 125.67MB read
### Requests/sec:  35655.59
### Transfer/sec:      4.18MB